//
// Created by vlaser
//

#include <string.h>
#include <stdio.h>
#include "command_encoder.h"

#define HALF_BYTE_MASK 15u
#define MOV_CMD_SHIFT 5u

char encode_operands(const OpStruct *op1, const OpStruct *op2) {
    char type1 = op1->type;
    if (type1 == IMM8) {
        return op1->value;
    }

    char res = (op1->value << 4) + op2->value;
    return res;
}

void encode_mov(const CmdStruct* cmd, char encoded[]) {

    char type = cmd->commandType << MOV_CMD_SHIFT;
    char op;

    if (cmd->commandType == MOVC) {
        type += cmd->op1.value & HALF_BYTE_MASK;
        op = cmd->op2.value;
    } else {
        op = encode_operands(&cmd->op1, &cmd->op2);
    }
    encoded[0] = type;
    encoded[1] = op;
}

void encode_command(const CmdStruct* cmd, char encoded[]) {
    int type = cmd->commandType;

    if (type <= MOVC) {
        encode_mov(cmd, encoded);
    } else {
        encoded[0] = cmd->commandType;
        encoded[1] = encode_operands(&cmd->op1, &cmd->op2);
    }
}

void encode_prog(const CmdStruct *commands, char *result, size_t command_count) {
    size_t result_index = 0;
    for (size_t i = 0; i != command_count; ++i, result_index += 2) {
        char encoded_cmd[2];
        encode_command(&commands[i], encoded_cmd);
        memcpy(&result[result_index], encoded_cmd, 2);
    }
}
