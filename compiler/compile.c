//
// Created by vlaser
//

#include <glob.h>
#include <stdio.h>
#include <stdlib.h>
/**
 * принимает на вход файл, который надо скомпилировать
 * и имя файла в который надо результат написать
 *
 * Использует
 * - Чтения из файла
 * - Запись в файл
 * - Парсер
 * - Кодировщик, исходной программы в байт код
 */

#include "../utils/file_utils.h"
#include "../utils/utils.h"
#include "asm_parser.h"
#include "command_encoder.h"

#define MAXSIZE 100500

int main(int ac, char *av[]) {

    char data[MAXSIZE];

    if (ac < 3) {
        return 0;
    }

    size_t result_size = read_from_file(av[1], data);
    printf("%zu \n", result_size);

    size_t line_count = get_line_count(data);
    printf("%zu \n", line_count);

    CmdStruct *commands = malloc(sizeof(commands[0]) * line_count);
    if (!commands) {
        return 1;
    }

    int rv = parse_commands(data, commands, line_count);
    if (rv != 0) {
        goto free_commands;
    }

    char* encoded = malloc(sizeof(encoded[0]) * line_count * 2);
    if (!encoded) {
        goto free_commands;
    }

    encode_prog(commands, encoded, line_count);
    size_t write_size = write_to_binary_file(av[2], encoded, line_count * 2);
    printf("%zu \n", write_size);

    free(encoded);

    free_commands:
    free(commands);

    return 0;
}
