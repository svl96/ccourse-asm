//
// Created by vlaser
//
#ifndef CCOURCE_ASM_ENTITIES_H
#define CCOURCE_ASM_ENTITIES_H

enum OperandType {
    EMPTY = 0,
    RX = 1,
    RS = 2,
    IMM4 = 3,
    IMM8 = 4,
};

enum CommandType {
    MOV = 1,
    MTM = 2, // Move to memory
    MFM = 3, // Move from memory
    MOVC = 4, // Move constant
    PUSH = 5,
    POP = 6,
    CALL = 7,
    RET = 8,
    ADD = 9,
    SUB = 10,
    MUL = 11,
    DIV = 12,
    AND = 13,
    OR = 14,
    XOR = 15,
    NOT = 16,
    SHL = 17,
    SHR = 18,
    RES = 19,
    NOP = 20,
    JMP = 21,
    JE = 22,
    JNE = 23,
    IN = 24,
    OUT = 25,
    LAB = 26
};

struct Operand {
    char type;
    char value;
};

struct Command {
    char commandType;
    struct Operand op1;
    struct Operand op2;
};

typedef enum CommandType CmdType;

typedef struct Operand OpStruct;
typedef struct Command CmdStruct;


#endif // CCOURCE_ASM_ENTITIES_H
