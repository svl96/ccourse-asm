//
// Created by vlaser on 12/13/18.
//

#ifndef CCOURCE_ASM_ASM_PARSER_H
#define CCOURCE_ASM_ASM_PARSER_H

#include <stddef.h>
#include "entities.h"

struct ParsedCmd {
    char label[30];
    char str_cmd[10];
    char str_op1[30];
    char str_op2[30];
};

CmdType extract_command_type(const char *cmd);

int is_rs(const char op_str[]);

int is_rx(const char op_str[]);

int is_rx_mem(const char op_str[]);

int is_imm_operand(const char *op_str);

int extract_mov_operands(CmdStruct *cmd, const char op1[], const char op2[]);

int extract_mem_rx(OpStruct *op, const char *op_s);

int extract_rx(OpStruct *op, const char *op_s);

int extract_rs(OpStruct *op, const char *op_s);

int extract_imm(OpStruct *op, const char *op_s, unsigned len, char is_signed);

int get_jmp_diff(OpStruct *op, const char label[], struct ParsedCmd cmd[],
        size_t position, size_t command_count);

int create_command(CmdStruct *comm, struct ParsedCmd parsedCmd[],
        size_t position, size_t command_count);

void format_label(char* label);

size_t parse_command(struct ParsedCmd parsed[], const char* program, size_t position);

int parse_commands(const char* program, CmdStruct commands[], size_t commands_count);

#endif //CCOURCE_ASM_ASM_PARSER_H
