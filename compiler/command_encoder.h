//
// Created by vlaser on 12-Dec-18.
//

#ifndef CCOURCE_ASM_COMMAND_ENCODER_H
#define CCOURCE_ASM_COMMAND_ENCODER_H

#include "entities.h"
#include <stddef.h>

char encode_operands(const OpStruct *op1, const OpStruct *op2);

void encode_mov(const CmdStruct* cmd, char encoded[]);

void encode_command(const CmdStruct* cmd, char encoded[]);

void encode_prog(const CmdStruct *commands, char *result, size_t command_count);

#endif //CCOURCE_ASM_COMMAND_ENCODER_H
