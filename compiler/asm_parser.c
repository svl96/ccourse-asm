//
// Created by vlaser on 12-Dec-18.
//

#include "asm_parser.h"
#include <malloc.h>
#include <string.h>
#include <stdlib.h>

#define EQUALS(str1, str2) strcmp(str1, str2) == 0

#define IMM8_LEN 8u
#define IMM4_LEN 4u
#define BYTE_MASK 7u

#define CHECK_CMD(cmd1, cmd2, type, res) \
    if (EQUALS(cmd1, cmd2)) { res = type; }

#define CHECK_EXTRACT(rv, label) \
    if ((rv) != 0) goto label;

#define IS_LOW(ch) (ch == 'l' || ch == 'L')
#define IS_HIGH(ch) (ch == 'h' || ch == 'H')


CmdType extract_command_type(const char *cmd) {
    if (strlen(cmd) == 0) {
        return 0;
    }
    CmdType result_cmd = -1;

    CHECK_CMD(cmd, "mov", MOV, result_cmd);
    CHECK_CMD(cmd, "push", PUSH, result_cmd);
    CHECK_CMD(cmd, "pop", POP, result_cmd);
    CHECK_CMD(cmd, "call", CALL, result_cmd);
    CHECK_CMD(cmd, "ret", RET, result_cmd);
    CHECK_CMD(cmd, "add", ADD, result_cmd);
    CHECK_CMD(cmd, "sub", SUB, result_cmd);
    CHECK_CMD(cmd, "mul", MUL, result_cmd);
    CHECK_CMD(cmd, "div", DIV, result_cmd);
    CHECK_CMD(cmd, "and", AND, result_cmd);
    CHECK_CMD(cmd, "or", OR, result_cmd);
    CHECK_CMD(cmd, "xor", XOR, result_cmd);
    CHECK_CMD(cmd, "not", NOT, result_cmd);
    CHECK_CMD(cmd, "shl", SHL, result_cmd);
    CHECK_CMD(cmd, "shr", SHR, result_cmd);
    CHECK_CMD(cmd, "reset", RES, result_cmd);
    CHECK_CMD(cmd, "nop", NOP, result_cmd);
    CHECK_CMD(cmd, "jmp", JMP, result_cmd);
    CHECK_CMD(cmd, "je", JE, result_cmd);
    CHECK_CMD(cmd, "jne", JNE, result_cmd);
    CHECK_CMD(cmd, "in", IN, result_cmd);
    CHECK_CMD(cmd, "out", OUT, result_cmd);

    if (result_cmd < 0) {
        fprintf(stderr, "Command %s doesn\'t exists \n", cmd);
    }
    return result_cmd;
}

void set_empty_op(OpStruct *op) {
    op->type = (char)(EMPTY & BYTE_MASK);
    op->value = 0;
}

int is_rs(const char op_str[]) {
    return strlen(op_str) == 3 && \
            (op_str[0] == 'r' || op_str[0] == 'R') && \
            (op_str[1] >= '0' && op_str[1] <= '7') && \
            (IS_LOW(op_str[2]) || IS_HIGH(op_str[2]));
}

int is_rx(const char op_str[]) {
    if (strlen(op_str) == 2 &&
        (EQUALS(op_str, "SP") || EQUALS(op_str, "sp"))) {
        return 1;
    }

    return strlen(op_str) == 3 && \
            (op_str[0] == 'r' || op_str[0] == 'R') && \
            (op_str[1] >= '0' && op_str[1] <= '7') && \
            (op_str[2] == 'x' || op_str[2] == 'X');
}

int is_rx_mem(const char op_str[]) {
    if (strlen(op_str) != 5 || op_str[0] != '(' || op_str[4] != ')') {
        return 0;
    }
    char reg[4];
    strncpy(reg, &op_str[1], 3);
    reg[3] = 0;
    return is_rx(reg);
}

int is_imm_operand(const char *op_str) {
    if (strlen(op_str) == 0 || op_str[0] != '#') {
        return 0;
    }
    long num = strtol(&op_str[1], NULL, 10);
    return num >= -128 && num <= 255;
}

int extract_mem_rx(OpStruct *op, const char *op_str) {
    int rv = 0;
    if (strlen(op_str) != 5 || op_str[0] != '(' || op_str[4] != ')') {
        rv = 1;
        goto fail;
    }
    char reg[4];
    strncpy(reg, &op_str[1], 3);
    reg[3] = 0;
    CHECK_EXTRACT(rv = extract_rx(op, reg), fail);
    return rv;

    fail:
    fprintf(stderr, "Invalid register %s\n", op_str);
    return rv;
}

int extract_mov_operands(CmdStruct *comm, const char op1_str[], const char op2_str[]) {
    int rv = 0;
    if (is_rx(op1_str) && is_rx(op2_str)) {
        comm->commandType = MOV & BYTE_MASK;
        rv = extract_rx(&comm->op1, op1_str);
        rv = rv != 0 ? rv : extract_rx(&comm->op2, op2_str);

    } else if (is_rx_mem(op1_str) && is_rx(op2_str)) {
        comm->commandType = MTM & BYTE_MASK;
        rv = extract_mem_rx(&comm->op1, op1_str);
        rv = rv != 0 ? rv : extract_rx(&comm->op2, op2_str);

    } else if (is_rx(op1_str) && is_rx_mem(op2_str)) {
        comm->commandType = MFM & BYTE_MASK;
        rv = extract_rx(&comm->op1, op1_str);
        rv = rv != 0 ? rv : extract_mem_rx(&comm->op2, op2_str);

    } else if (is_rs(op1_str) && is_imm_operand(op2_str)) {
        comm->commandType = MOVC &BYTE_MASK;
        rv = extract_rs(&comm->op1, op1_str);
        rv = rv != 0 ? rv : extract_imm(&comm->op2, op2_str, IMM8_LEN, 0);
    }

    if (rv != 0) {
        fprintf(stderr, "Invalid command mov \n");
    }
    return rv;
}

int extract_rx(OpStruct *op, const char *op_str) {
    if (!is_rx(op_str)) {
        fprintf(stderr, "Invalid register %s\n", op_str);
        return 1;
    }
    op->type = RX & BYTE_MASK;
    op->value = ((EQUALS("sp", op_str) || EQUALS("SP", op_str))
            ? (char)7
            : (char)(op_str[1] - '0'));
    return 0;
}

int extract_rs(OpStruct *op, const char *op_str) {
    if (!is_rs(op_str)) {
        fprintf(stderr, "Invalid register %s\n", op_str);
        return 1;
    }
    char shift = (char) (IS_HIGH(op_str[2]) ? 0 : 1);
    op->type = RS & BYTE_MASK;
    op->value = (char)((op_str[1] - '0') << 1) + shift;
    return 0;
}

int extract_imm(OpStruct *op, const char *op_str, unsigned len, char is_signed) {
    char type;
    char value;
    if (is_imm_operand(op_str) == 0) {
        goto invalid;
    }

    long num = strtol(&op_str[1], NULL, 10);
    if (len == IMM4_LEN) {
        type = IMM4 & BYTE_MASK;
        if (num < 0 || num > 15) {
            goto invalid;
        }
        value = (char)num;
    } else {
        type = IMM8 & BYTE_MASK;
        if ((!is_signed && num < 0) || (is_signed && num > 127) ) {
            goto invalid;
        }
        value = (char)num;
    }
    op->type = type;
    op->value = value;
    return 0;

    invalid:
    fprintf(stderr, "Is not Invalid register %s\n", op_str);
    return 1;
}

int get_jmp_diff(OpStruct *op, const char label[], struct ParsedCmd parsed_cmds[],
        size_t position, size_t command_count) {
    // todo test this area
    char ch = 0;
    for (size_t i = 0; i != command_count; ++i) {
        struct ParsedCmd parsed_cmd = parsed_cmds[i];
        if (EQUALS(label, parsed_cmd.label)) {
            long diff = i - position;
            if (diff > 127 || diff < -128) {
                goto invalid;
            }
            ch = (char)diff;
            break;
        }
    }
    op->type = IMM8;
    op->value = ch;
    return 0;

    invalid:
    fprintf(stderr, "%zu: Assemble error jmp to out of range", position);
    return 1;
}

int create_command(CmdStruct *comm, struct ParsedCmd parsedCmds[], size_t position, size_t commands_count) {
    // todo test this area
    int rv = 0;
    comm->commandType = 0;
    set_empty_op(&comm->op1);
    set_empty_op(&comm->op2);
    struct ParsedCmd parsedCmd = parsedCmds[position];
    int cmd = extract_command_type(parsedCmd.str_cmd);
    if (cmd < 0) {
        return 1;
    }
    comm->commandType = (CmdType)cmd;
    switch (cmd) {
        case MOV:
            rv = extract_mov_operands(comm, parsedCmd.str_op1, parsedCmd.str_op2);
            break;
        case PUSH:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            break;
        case POP:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            break;
        case CALL:
            rv = extract_imm(&comm->op1, parsedCmd.str_op1, IMM8_LEN, 1);
            break;
        case RET:
            break;
        case ADD:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case SUB:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case MUL:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case DIV:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case AND:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case OR:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case XOR:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case NOT:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            break;
        case SHL:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_imm(&comm->op2, parsedCmd.str_op2, IMM4_LEN, 0);
            break;
        case SHR:
            rv = extract_rx(&comm->op1, parsedCmd.str_op1);
            rv = rv ? rv : extract_imm(&comm->op2, parsedCmd.str_op2, IMM4_LEN, 0);
            break;
        case RES:
            break;
        case NOP:
            break;
        case JMP:
            rv = get_jmp_diff(&comm->op1, parsedCmd.str_op1, parsedCmds,
                    position, commands_count);
            break;
        case JNE:
            rv = get_jmp_diff(&comm->op1, parsedCmd.str_op1, parsedCmds, position, commands_count);
            rv = extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case JE:
            rv = get_jmp_diff(&comm->op1, parsedCmd.str_op1, parsedCmds, position, commands_count);
            rv = extract_rx(&comm->op2, parsedCmd.str_op2);
            break;
        case IN:
            rv = extract_rs(&comm->op1, parsedCmd.str_op1);
            break;
        case OUT:
            rv = extract_rs(&comm->op1, parsedCmd.str_op1);
            break;
        default:
            break;
    }
    return rv;
}

void format_label(char* label) {
    size_t label_len = strlen(label);
    if (label_len > 1 && label[label_len - 1] == ':') {
        label[label_len - 1] = 0;
    }
}

size_t parse_command(struct ParsedCmd *parsed_cmd, const char* program, size_t position) {
    char ch = program[position];
    char* refs[] = {parsed_cmd->label, parsed_cmd->str_cmd, parsed_cmd->str_op1, parsed_cmd->str_op2};

    char is_active = 1;
    unsigned state = 0;
    size_t index = 0;
    while (ch && ch != '\n') {
        if (ch == ' ') {
            if (is_active != 0) {
                refs[state][index] = 0;
                ++state;
                is_active = 0;
                index = 0;
            }
        } else {
            if (state < 4) {
                refs[state][index] = ch;
                ++index;
                is_active = 1;
            } else {
                goto error;
            }
        }
        ++position;
        ch = program[position];
    }
    if (state < 4) {
        refs[state][index] = 0;
    }
    ++position;
    format_label(parsed_cmd->label);
    return position;

    error:
        fprintf(stderr, "Error: to many commands in line");
        exit(1);
}

int parse_commands(const char* program, CmdStruct commands[], size_t commands_count) {

    // todo test this
    int rv = 0;
    struct ParsedCmd *parsed_cmds = malloc(sizeof(parsed_cmds[0]) * commands_count);
    if (!parsed_cmds) {
        return 1;
    }
    size_t position = 0;
    for (size_t i = 0; i != commands_count; ++i) {
        struct ParsedCmd parsed_cmd;
        position = parse_command(&parsed_cmd, program, position);
        parsed_cmds[i] = parsed_cmd;
    }

    for (size_t i = 0; i != commands_count; ++i) {
        rv = create_command(&commands[i], parsed_cmds, i, commands_count);
        if (rv != 0) {
            break;
        }
    }

    free(parsed_cmds);
    return rv;
}
