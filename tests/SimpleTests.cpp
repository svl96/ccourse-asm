//
// Created by vlaser
//
extern "C" {
    #include "../utils/utils.h"
}

#include "gtest/gtest.h"
#include <string>

TEST(SimpleTests, UtilTestLineCounter) {

    const char* lines = "abd\nasdf\n\nasdf   \n   asdfasdf \n";
    size_t count = get_line_count(lines);

    EXPECT_EQ(5, count);
}

TEST(SimpleTest, CharInitTest) {
    char a = 13;
    char b = 7u;
    a = a & b;
    size_t len = strlen("1234");
    EXPECT_EQ(4, len);
}
