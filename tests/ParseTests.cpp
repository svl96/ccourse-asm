//
// Created by vlaser
//
extern "C" {
    #include "../compiler/asm_parser.h"
}
#include "gtest/gtest.h"
#include <string>

TEST(ParseTest, ExtractCmd) {
    const char* mov = "mov";
    const char* push = "push";
    const char* reset = "reset";
    const char* inv = "inv";

    CmdType mov_type = extract_command_type(mov);
    CmdType push_type = extract_command_type(push);
    CmdType reset_type = extract_command_type(reset);
    int invalid = extract_command_type(inv);

    EXPECT_EQ(MOV, mov_type);
    EXPECT_EQ(PUSH, push_type);
    EXPECT_EQ(RES, reset_type);
    EXPECT_NE(PUSH, reset_type);
    EXPECT_EQ(-1, invalid);
}

TEST(ParseTest, GetRX) {
    const char* r0x = "R0X";
    const char* r1x = "R1X";
    const char* r7x = "R7X";
    const char* sp = "SP";
    const char* r8x = "R8X";

    OpStruct r0_res;
    OpStruct r1_res;
    OpStruct r7_res;
    OpStruct sp_res;
    OpStruct r8_res;
    int rv0 = extract_rx(&r0_res, r0x);
    int rv1 = extract_rx(&r1_res, r1x);
    int rv7 = extract_rx(&r7_res, r7x);
    int rv_sp = extract_rx(&sp_res, sp);
    int rv8 = extract_rx(&r8_res, r8x);

    EXPECT_EQ(RX, r0_res.type);
    EXPECT_EQ(RX, sp_res.type);
    EXPECT_EQ(0, rv0);
    EXPECT_EQ(0, rv1);
    EXPECT_EQ(0, rv7);
    EXPECT_EQ(0, rv_sp);
    EXPECT_NE(0, rv8);

    EXPECT_EQ(0, r0_res.value);
    EXPECT_EQ(1, r1_res.value);
    EXPECT_EQ(7, r7_res.value);
    EXPECT_EQ(7, sp_res.value);
}

TEST(ParseTest, GetRS) {
    const char* r0l = "R0L";
    const char* r1h = "R1H";
    const char* r5h = "r5h";
    const char* r8h = "R8H";

    OpStruct r0_res;
    OpStruct r1_res;
    OpStruct r5_res;
    OpStruct r8_res;

    int rv0 = extract_rs(&r0_res, r0l);
    int rv1 = extract_rs(&r1_res, r1h);
    int rv5 = extract_rs(&r5_res, r5h);
    int rv8 = extract_rs(&r8_res, r8h);

    EXPECT_EQ(RS, r0_res.type);
    EXPECT_EQ(RS, r5_res.type);
    EXPECT_EQ(0, rv0);
    EXPECT_EQ(0, rv1);
    EXPECT_EQ(0, rv5);
    EXPECT_EQ(1, rv8);

    EXPECT_EQ(1, r0_res.value);
    EXPECT_EQ(2, r1_res.value);
    EXPECT_EQ(10, r5_res.value);
}

TEST(ParseTest, GetImm) {
    const char* x1 = "#7";
    const char* x2 = "#255";
    const char* x3 = "#-10";
    const char* x4 = "#-130";

    OpStruct x1_res;
    int r1 = extract_imm(&x1_res, x1, 4, 0);
    OpStruct x1_res_big;
    int r1_big = extract_imm(&x1_res_big, x1, 8, 0);

    OpStruct x2_res;
    int r2 = extract_imm(&x2_res, x2, 8, 0);


    OpStruct x2_res_fail_sign;
    int r2_fail_sign = extract_imm(&x2_res_fail_sign, x2, 8, 1);

    OpStruct x2_res_fail_size;
    int r2_fail_size = extract_imm(&x2_res_fail_size, x2, 4, 0);

    OpStruct x3_res;
    int r3 = extract_imm(&x3_res, x3, 8, 1);

    OpStruct x3_res_fail;
    int r3_fail = extract_imm(&x3_res_fail, x3, 8, 0);

    OpStruct x4_res;
    int r4 = extract_imm(&x4_res, x4, 8, 1);

    EXPECT_EQ(0, r1);
    EXPECT_EQ(0, r1_big);
    EXPECT_EQ(0, r2);
    EXPECT_EQ(0, r3);

    EXPECT_NE(0, r2_fail_sign);
    EXPECT_NE(0, r2_fail_size);
    EXPECT_NE(0, r3_fail);
    EXPECT_NE(0, r4);

    EXPECT_EQ((char)IMM4, x1_res.type);
    EXPECT_EQ((char)IMM8, x1_res_big.type);
    EXPECT_EQ((char)IMM8, x2_res.type);

    EXPECT_EQ((char)7, x1_res.value);
    EXPECT_EQ((char)7, x1_res_big.value);
    EXPECT_EQ((char)255, x2_res.value);
    EXPECT_EQ((char)(256 - 10), x3_res.value);
}

TEST(ParseTest, GetSimpleMov) {
    CmdStruct comm = {};
    const char *op1_str = "R0X";
    const char *op2_str = "R2X";

    int rv = extract_mov_operands(&comm, op1_str, op2_str);

    EXPECT_EQ(0, rv);
    EXPECT_EQ((char)MOV, comm.commandType);
    EXPECT_EQ((char)RX, comm.op1.type);
    EXPECT_EQ((char)RX, comm.op2.type);
    EXPECT_EQ((char)0, comm.op1.value);
    EXPECT_EQ((char)2, comm.op2.value);

}

TEST(ParseTest, GetSimpleMovToMem) {
    CmdStruct comm = {};
    const char *op1_str = "(R0X)";
    const char *op2_str = "R2X";

    int rv = extract_mov_operands(&comm, op1_str, op2_str);

    EXPECT_EQ(0, rv);
    EXPECT_EQ((char)MTM, comm.commandType);
    EXPECT_EQ((char)RX, comm.op1.type);
    EXPECT_EQ((char)RX, comm.op2.type);
    EXPECT_EQ((char)0, comm.op1.value);
    EXPECT_EQ((char)2, comm.op2.value);
}

TEST(ParseTest, GetSimpleMovFromMem) {
    CmdStruct comm = {};
    const char *op1_str = "R0X";
    const char *op2_str = "(R2X)";

    int rv = extract_mov_operands(&comm, op1_str, op2_str);

    EXPECT_EQ(0, rv);
    EXPECT_EQ((char)MFM, comm.commandType);
    EXPECT_EQ((char)RX, comm.op1.type);
    EXPECT_EQ((char)RX, comm.op2.type);
    EXPECT_EQ((char)0, comm.op1.value);
    EXPECT_EQ((char)2, comm.op2.value);
}

TEST(ParseTest, GetSimpleMovConst) {
    CmdStruct comm = {};
    const char *op1_str = "R0L";
    const char *op2_str = "#123";

    int rv = extract_mov_operands(&comm, op1_str, op2_str);

    EXPECT_EQ(0, rv);
    EXPECT_EQ((char)MOVC, comm.commandType);
    EXPECT_EQ((char)RS, comm.op1.type);
    EXPECT_EQ((char)IMM8, comm.op2.type);
    EXPECT_EQ((char)1, comm.op1.value);
    EXPECT_EQ((char)123, comm.op2.value);
}

TEST(ParseTest, FormatLabel) {
    char label[7] = {'l','a','b','e','l',':', '\0'};
    char label1[6] = {'l','a','b','e','l', '\0'};
    char label2[1] = {'\0'};

    format_label(label);
    format_label(label1);
    format_label(label2);

    EXPECT_EQ("label", (std::string)label);
    EXPECT_EQ("label", (std::string)label1);
    EXPECT_EQ("", (std::string)label2);
}


TEST(ParseTest, ParseCommand) {
    struct ParsedCmd parsed1 = {};
    struct ParsedCmd parsed2 = {};

    const char* program = " mov R0X R0X \nlabel: add R0X R5X ";
    size_t position = 0;
    position = parse_command(&parsed1, program, position);
    EXPECT_EQ(14, position);
    position = parse_command(&parsed2, program, position);
    EXPECT_EQ(34, position);

    EXPECT_EQ("", (std::string)parsed1.label);
    EXPECT_EQ("mov", (std::string)parsed1.str_cmd);
    EXPECT_EQ("R0X", (std::string)parsed1.str_op1);
    EXPECT_EQ("R0X", (std::string)parsed1.str_op2);

    EXPECT_EQ("label", (std::string)parsed2.label);
    EXPECT_EQ("add", (std::string)parsed2.str_cmd);
    EXPECT_EQ("R0X", (std::string)parsed2.str_op1);
    EXPECT_EQ("R5X", (std::string)parsed2.str_op2);
}

TEST(ParseTest, ParseCommands) {
    const char* program = " mov R0X R2X \nlabel: add R0X R5X \n";
    CmdStruct commands[2];
    size_t count = 2;

    int rv = parse_commands(program, commands, count);

    EXPECT_EQ(0, rv);

    CmdStruct cmd1 = commands[0];
    CmdStruct cmd2 = commands[1];

    EXPECT_EQ((char)MOV, cmd1.commandType);
    EXPECT_EQ((char)RX, cmd1.op1.type);
    EXPECT_EQ((char)RX, cmd1.op2.type);
    EXPECT_EQ((char)0, cmd1.op1.value);
    EXPECT_EQ((char)2, cmd1.op2.value);


    EXPECT_EQ((char)ADD, cmd2.commandType);
    EXPECT_EQ((char)RX, cmd2.op1.type);
    EXPECT_EQ((char)RX, cmd2.op2.type);
    EXPECT_EQ((char)0, cmd2.op1.value);
    EXPECT_EQ((char)5, cmd2.op2.value);
}

TEST(ParseTest, ParseCommandsWithLabel) {
    const char* program = " je label R2X \n mov R0X R2X \nlabel: add R0X R5X \n";
    size_t count = 3;
    CmdStruct commands[3];

    int rv = parse_commands(program, commands, count);

    EXPECT_EQ(0, rv);

    CmdStruct cmd1 = commands[0];
    CmdStruct cmd2 = commands[2];

    EXPECT_EQ((char)JE, cmd1.commandType);
    EXPECT_EQ((char)IMM8, cmd1.op1.type);
    EXPECT_EQ((char)RX, cmd1.op2.type);
    EXPECT_EQ((char)2, cmd1.op1.value);
    EXPECT_EQ((char)2, cmd1.op2.value);


    EXPECT_EQ((char)ADD, cmd2.commandType);
    EXPECT_EQ((char)RX, cmd2.op1.type);
    EXPECT_EQ((char)RX, cmd2.op2.type);
    EXPECT_EQ((char)0, cmd2.op1.value);
    EXPECT_EQ((char)5, cmd2.op2.value);
}

TEST(ParseTest, ParseCommandsWithJmpLabel) {
    const char* program = " jmp label \n mov R0X R2X \nlabel: add R0X R5X \n";
    size_t count = 3;
    CmdStruct commands[3];



    int rv = parse_commands(program, commands, count);

    EXPECT_EQ(0, rv);

    CmdStruct cmd1 = commands[0];
    CmdStruct cmd2 = commands[2];

    EXPECT_EQ((char)JMP, cmd1.commandType);
    EXPECT_EQ((char)IMM8, cmd1.op1.type);
    EXPECT_EQ((char)2, cmd1.op1.value);
    EXPECT_EQ((char)EMPTY, cmd1.op2.type);
    EXPECT_EQ((char)0, cmd1.op2.value);


    EXPECT_EQ((char)ADD, cmd2.commandType);
    EXPECT_EQ((char)RX, cmd2.op1.type);
    EXPECT_EQ((char)RX, cmd2.op2.type);
    EXPECT_EQ((char)0, cmd2.op1.value);
    EXPECT_EQ((char)5, cmd2.op2.value);
}
TEST(ParseTest, ParseCommandsWithoutOperands) {
    const char* program = "lab: nop \n ret \n call #10 \n";
    size_t count = 3;
    CmdStruct commands[3];

    int rv = parse_commands(program, commands, count);
    EXPECT_EQ(0, rv);

    CmdStruct cmd1 = commands[0];
    CmdStruct cmd2 = commands[1];
    CmdStruct cmd3 = commands[2];

    EXPECT_EQ((char)NOP, cmd1.commandType);
    EXPECT_EQ((char)EMPTY, cmd1.op1.type);
    EXPECT_EQ((char)EMPTY, cmd1.op2.type);
    EXPECT_EQ((char)0, cmd1.op1.value);
    EXPECT_EQ((char)0, cmd1.op2.value);

    EXPECT_EQ((char)RET, cmd2.commandType);
    EXPECT_EQ((char)EMPTY, cmd2.op1.type);
    EXPECT_EQ((char)EMPTY, cmd2.op2.type);
    EXPECT_EQ((char)0, cmd2.op1.value);
    EXPECT_EQ((char)0, cmd2.op2.value);


    EXPECT_EQ((char)CALL, cmd3.commandType);
    EXPECT_EQ((char)IMM8, cmd3.op1.type);
    EXPECT_EQ((char)EMPTY, cmd3.op2.type);
    EXPECT_EQ((char)10, cmd3.op1.value);
    EXPECT_EQ((char)0, cmd3.op2.value);
}