//
// Created by vlaser
//

extern "C" {
    #include "../compiler/command_encoder.h"
}

#include "gtest/gtest.h"
#include <string>

TEST(EncoderTests, SimpleOperandsEncoder) {

    const OpStruct op1 = {RX, 0};
    const OpStruct op2 = {RX, 1};
    char res = encode_operands(&op1, &op2);
    EXPECT_EQ((char)1, res);
}

TEST(EncoderTests, SimpleOperandsEncoder2) {

    const OpStruct op1 = {RX, 1};
    const OpStruct op2 = {RX, 0};
    char res = encode_operands(&op1, &op2);
    EXPECT_EQ((char)16, res);
}

TEST(EncoderTests, SimpleMovEncoder) {

    OpStruct op1 = {RX, 0};
    OpStruct op2 = {RX, 1};
    const CmdStruct cmd = {MOV, op1, op2};
    char encoded[2] = {0, 0};
    encode_mov(&cmd, encoded);

    EXPECT_EQ((char)32, encoded[0]);
    EXPECT_EQ((char)1, encoded[1]);
}

TEST(EncoderTests, SimpleMovEncoder2) {

    OpStruct op1 = {RX, 0};
    OpStruct op2 = {RX, 1};
    const CmdStruct cmd = {MTM, op1, op2};
    char encoded[2] = {0, 0};
    encode_mov(&cmd, encoded);

    EXPECT_EQ((char)64, encoded[0]);
    EXPECT_EQ((char)1, encoded[1]);
}

TEST(EncoderTests, SimpleMovEncoder3) {

    OpStruct op1 = {RX, 0};
    OpStruct op2 = {RX, 1};
    const CmdStruct cmd = {MFM, op1, op2};
    char encoded[2] = {0, 0};
    encode_mov(&cmd, encoded);

    EXPECT_EQ((char)(96), encoded[0]);
    EXPECT_EQ((char)1, encoded[1]);
}

TEST(EncoderTests, SimpleMovEncoder4) {

    OpStruct op1 = {RS, 1};
    OpStruct op2 = {IMM8, (char)133u};
    const CmdStruct cmd = {MOVC, op1, op2};
    char encoded[2] = {0, 0};
    encode_mov(&cmd, encoded);

    EXPECT_EQ((char)(129), encoded[0]);
    EXPECT_EQ((char)(133), encoded[1]);
}

TEST(EncoderTests, SimpleEncoder) {

    OpStruct op1 = {RX, 1u};
    OpStruct op2 = {RX, 7u};
    const CmdStruct cmd = {MUL, op1, op2};
    char encoded[2] = {0, 0};
    encode_command(&cmd, encoded);

    EXPECT_EQ((char)(MUL), encoded[0]);
    EXPECT_EQ((char)(23), encoded[1]);
}

TEST(EncoderTests, SimpleEncoderProg) {
    CmdStruct cmds[3];
    cmds[0] = {ADD, {RX, 1u}, {RX, 7u}};
    cmds[1] = {MOV, {RX, 1u}, {RX, 5u}};
    cmds[2] = {NOT, {RX, 3u}, {EMPTY, 0}};
    int count = 3;


    char encoded[6];
    encode_prog(cmds, encoded, count);

    EXPECT_EQ((char)ADD, encoded[0]);
    EXPECT_EQ((char)23, encoded[1]);

    EXPECT_EQ((char)(MOV << 5), encoded[2]);
    EXPECT_EQ((char)21, encoded[3]);

    EXPECT_EQ((char)NOT, encoded[4]);
    EXPECT_EQ((char)48, encoded[5]);
}
