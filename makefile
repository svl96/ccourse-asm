CC=/usr/bin/clang
CXX=/usr/bin/clang++
LD=/usr/bin/clang++
LLVM_PROFDATA=/usr/bin/llvm-profdata
LLVM_COV=/usr/bin/llvm-cov

COVERAGE_FORMAT=report

GTEST=googletest/googletest
COMPILE_TESTS=tests
UTILS_PATH=utils
COMPILER_PATH=compiler

INCLUDES=-I$(GTEST) -I$(GTEST)/include
WARN_OPTS=-Wall -Werror -pedantic

COVERAGE=-fprofile-instr-generate -fcoverage-mapping

CFLAGS=$(WARN_OPTS) $(INCLUDES) -std=c11 $(COVERAGE)
CXXFLAGS=$(WARN_OPTS) $(INCLUDES) -std=c++17 $(COVERAGE)

LDFLAGS= -lm -lpthread $(COVERAGE)

clean:
	find . -name '*.o' -delete

all: Main TestCompiler

Main: $(COMPILER_PATH)/compile.o $(COMPILER_PATH)/asm_parser.o \
		$(UTILS_PATH)/utils.o $(UTILS_PATH)/file_utils.o \
		$(COMPILER_PATH)/command_encoder.o
	$(CC) $(CFLAGS) -o $@ $^

CompilerProg: $(COMPILER_PATH)/asm_parser.o $(UTILS_PATH)/utils.o $(UTILS_PATH)/file_utils.o $(COMPILER_PATH)/command_encoder.o

$(COMPILER_PATH)/compile.o: $(COMPILER_PATH)/compile.c
$(COMPILER_PATH)/asm_parser.o: $(COMPILER_PATH)/asm_parser.c
$(COMPILER_PATH)/command_encoder.o: $(COMPILER_PATH)/command_encoder.c

$(UTILS_PATH)/utils.o: $(UTILS_PATH)/utils.c
$(UTILS_PATH)/file_utils.o: $(UTILS_PATH)/file_utils.c

TestCompiler: \
		$(COMPILER_PATH)/command_encoder.o \
		$(COMPILER_PATH)/asm_parser.o \
		$(UTILS_PATH)/utils.o \
		$(GTEST)/src/gtest-all.o \
		$(GTEST)/src/gtest_main.o \
		$(COMPILE_TESTS)/SimpleTests.o \
		$(COMPILE_TESTS)/ParseTests.o \
		$(COMPILE_TESTS)/EncoderTests.o
	$(CXX) $(LDFLAGS) -o $@ $^

$(GTEST)/src/gtest-all.o: $(GTEST)/src/gtest-all.cc

$(GTEST)/src/gtest_main.o: $(GTEST)/src/gtest_main.cc

$(COMPILE_TESTS)/SimpleTests.o: $(COMPILE_TESTS)/SimpleTests.cpp

$(COMPILE_TESTS)/ParseTests.o: $(COMPILE_TESTS)/ParseTests.cpp

$(COMPILE_TESTS)/EncoderTests.o: $(COMPILE_TESTS)/EncoderTests.cpp

# coverage: TestNumberSystem
#	LLVM_PROFILE_FILE=$^.profraw ./$^
#	$(LLVM_PROFDATA) merge -sparse $^.profraw -o $^.profdata
#	$(LLVM_COV) $(COVERAGE_FORMAT) -name=MathOperations \
#		-instr-profile=$^.profdata $^
#		-ignore-filename-regex=$(GTEST) \
