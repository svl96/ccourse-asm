//
// Created by vlaser on 12/13/18.
//

#ifndef CCOURCE_ASM_UTILS_H
#define CCOURCE_ASM_UTILS_H

#include <stddef.h>

size_t get_line_count(const char data[]);

#endif //CCOURCE_ASM_UTILS_H
