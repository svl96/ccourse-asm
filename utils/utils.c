//
// Created by vlaser on 12/13/18.
//

#include "utils.h"

size_t get_line_count(const char data[]) {
    size_t i = 0;
    char ch = data[i];
    size_t counter = 0;

    while (ch) {
        if (ch == '\n') {
            ++counter;
        }

        ++i;
        ch = data[i];
    }

    return counter;
}
