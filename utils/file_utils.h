//
// Created by vlaser on 12-Dec-18.
//

#ifndef CCOURCE_ASM_FILE_READER_H
#define CCOURCE_ASM_FILE_READER_H

#include <stddef.h>

size_t read_from_file(char* filename, char* result);

size_t read_from_binary_file(char* filename, char* result);

size_t write_to_file_by_mode(char* filename, char* data, size_t data_size, char* mode);

size_t write_to_file(char* filename, char* data, size_t data_size);

size_t write_to_binary_file(char* filename, char* data, size_t data_size);


#endif //CCOURCE_ASM_FILE_READER_H
