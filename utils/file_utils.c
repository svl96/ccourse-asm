//
// Created by vlaser on 12-Dec-18.
//

#include "file_utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>


#define VERIFY(condition, operation, filename, rv, value, exit) \
do { \
    if (!(condition)) { \
        fprintf(stderr, "failed to %s %s: %s\n", \
                operation, filename, strerror(errno)); \
        rv = value; \
        exit; \
    } \
} while (0)

#define CHECK(condition, operation, filename, rv, value, label) \
    VERIFY(condition, operation, filename, rv, value, goto label)

#define ROLLBACK(condition, operation, filename, rv, value) \
    VERIFY(condition, operation, filename, rv, value, (void)0)



size_t read_from_file_by_mode(char* filename, char* result, char* mode) {

    FILE *fp;
    char buffer[1024];
    int rv = 0;
    size_t read_size;
    size_t offset = 0;

    CHECK(fp = fopen(filename, mode), "open", filename, rv, 1, exit);

    while (!feof(fp)) {
        read_size = fread(buffer, sizeof(buffer[0]), sizeof(buffer), fp);
        CHECK(!ferror(fp), "read from", filename, rv, 2, cleanup);

        memcpy(&result[offset], buffer, read_size);
        offset += read_size;
    }

    result[offset] = '\0';

    CHECK(fclose(fp) == 0, "close", filename, rv, 4, exit);

    cleanup:
        ROLLBACK(fclose(fp) == 0, "close", filename, rv, 5);

    return offset;

    exit:
        exit(rv);
}

size_t read_from_binary_file(char* filename, char* result) {
    return read_from_file_by_mode(filename, result, "rb");
}

size_t read_from_file(char* filename, char* result) {
    return read_from_file_by_mode(filename, result, "r");
}

size_t write_to_file_by_mode(char* filename, char* data, size_t data_size, char* mode) {
    int rv = 0;
    FILE* fp;

    CHECK(fp = fopen(filename, mode), "open", filename, rv, 1, exit);

    size_t written;
    CHECK(written = fwrite(data, sizeof(data[0]), data_size, fp),
            "write to", filename, rv, 2, exit);

    CHECK(fclose(fp) == 0, "close", filename, rv, 4, exit);

    return written;

    exit:
        exit(rv);
}

size_t write_to_file(char* filename, char* data, size_t data_size) {
    return write_to_file_by_mode(filename, data, data_size, "w");
}

size_t write_to_binary_file(char* filename, char* data, size_t data_size) {
    return write_to_file_by_mode(filename, data, data_size, "wb");
}
